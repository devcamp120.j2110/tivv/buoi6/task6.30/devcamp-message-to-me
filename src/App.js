import "bootstrap/dist/css/bootstrap.min.css";

import ContentComponent from "./components/content/ContentComponent";
import TitleComponent from "./components/title/TitleComponent";

function App() {
  return (
    <div className="container">
      <TitleComponent />
      <ContentComponent />
    </div>
  );
}

export default App;
