import { Component } from "react";
import titleImg from "../../../assets/images/title.png";

class TitleImage extends Component {
    render() {
        return (
            <div className="row" style={{marginBottom: 10}}> 
                <div className="col">
                <img src={titleImg} alt="Title" width={500} className="img-thumbnail"/>
                </div>
            </div>
        )
    }
}

export default TitleImage;