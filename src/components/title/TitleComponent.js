import { Component } from "react";
import TitleImage from "./title-image/TitleImage";
import TitleText from "./title-text/TitleText";

class TitleComponent extends Component {
    render() {
        return (
            <>
                <TitleText />
                <TitleImage />
            </>
        )
    }
}

export default TitleComponent;