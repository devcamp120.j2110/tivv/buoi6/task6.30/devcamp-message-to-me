import { Component } from "react";

class InputMessage extends Component {
    onChangeInput = (event) => {
        console.log("Nhập vào ô input");
        let inputValue = event.target.value;
        console.log(inputValue);

        this.props.changeInputMessageHandler(inputValue);
    }

    onClickBtn = () => {
        console.log("Bấm nút gửi thông điệp");
        console.log(this.props.inputMessage);

        this.props.changeMessageHandler(this.props.inputMessage);
    }

    render() {
        const { inputMessage } = this.props;

        return (
            <>
                <div className="row"> 
                    <div className="col">
                    <label className="form-label">Message cho bạn 12 tháng tới:</label>
                    </div>
                </div>
                <div className="row" style={{marginBottom: 20}}> 
                    <div className="col">
                    <input  value={inputMessage} onChange={this.onChangeInput} placeholder="Nhập message vào đây" className="form-control"></input>
                    </div>
                </div>
                <div className="row"> 
                    <div className="col">
                    <button className="btn btn-success" onClick={this.onClickBtn}>Gửi thông điệp</button>
                    </div>
                </div>
            </>
        )
    }
}

export default InputMessage;