import { Component } from "react";

import likeImg from "../../../assets/images/like.png";

class LikeImage extends Component {
    render() {
        const { message, likeDisplay } = this.props;

        return (
            <>
                <div className="row" style={{marginTop: 30}}> 
                    <div className="col">
                    {message.map((element, index) => {
                        return <p key={index}>{element}</p>
                    })}
                    </div>
                </div>
                <div className="row"> 
                    <div className="col">
                    <img src={likeImg} alt="Like" width={200} className="img-thumbnail" style={{display: likeDisplay}}/>
                    </div>
                </div>
            </>
        )
    }
}

export default LikeImage;