import { Component } from "react";
import InputMessage from "./input-message/InputMessage";
import LikeImage from "./like-image/LikeImage";

class ContentComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            inputMessage: "",
            message: [],
            likeDisplay: "none"
        }
    }

    changeInputMessage = (paramInputMessage) => {
        this.setState({
            inputMessage: paramInputMessage
        })
    }

    changeMessage = (paramMessage) => {
        if(paramMessage) {
            this.setState({
                message: [...this.state.message, paramMessage],
                likeDisplay: "block"
            });
        }
    }

    render() {
        return (
            <>
                <InputMessage inputMessage={this.state.inputMessage} changeInputMessageHandler={this.changeInputMessage} changeMessageHandler={this.changeMessage} />
                <LikeImage message={this.state.message} likeDisplay={this.state.likeDisplay} />
            </>
        )
    }
}

export default ContentComponent;